<?php

/* Template Name: Department */

function do_department_positions()
{
    global $post;

    $department = get_field('department', $post);


    $options = array(
        'post_type' => 'cmcareer',
        'post_status' => 'publish',
        'numberposts' => -1,
        //'exclude' => array(get_field('general_position_id', 'options')->ID)
    );


    $locations_array = array(
        'taxonomy' => 'location',
        'hide_empty' => true,
    );

    $options['tax_query'] = array(
        array(
            'taxonomy' => 'department',
            'field' => 'slug',
            'terms' => $department
        ),
    );


    $careers = get_posts($options);
    $locations = get_terms($locations_array);


    include(get_stylesheet_directory() . '/views/positions.php');

}

add_action('wp_enqueue_scripts', 'enqueue_all_positions_scripts');

function enqueue_all_positions_scripts()
{
    wp_enqueue_script('isotope', get_stylesheet_directory_uri() . '/assets/vendor/isotope.min.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_script('font-awesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/js/all.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_style('positions', get_stylesheet_directory_uri() . '/assets/css/department-colors.css');

}

add_action('genesis_entry_footer', 'do_department_positions', 6);
add_action('genesis_entry_footer', 'do_testimonial_slider', 7);


function do_testimonial_slider()
{
    global $post;
    $department = get_field('department', $post);

    $options = array(
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'numberposts' => -1,
        //'exclude' => array(get_field('general_position_id', 'options')->ID)
    );

    $options['tax_query'] = array(
        array(
            'taxonomy' => 'department',
            'field' => 'slug',
            'terms' => $department
        ),
    );
    $testimonials = get_posts($options);

    foreach ($testimonials as $testimonial) {

        $position = get_field('position', $testimonial);
        ?>
        <div class="testimonial-section">
            <div class="title">WHAT <strong>OUR TEAM</strong> THINKS ABOUT WORKING WITH US</div>
            <div class="testimonial-slider">
                <div class="testimonial">
                    <div class="header">
                        <div class="image"><img src="<?php echo get_the_post_thumbnail_url($testimonial); ?>"/> </div>
                        <div class="name"><?php echo $testimonial->post_title; ?></div>
                        <div class="position"><?php echo $position; ?></div>
                    </div>
                    <div class="text"><?php echo $testimonial->post_content; ?></div>
                </div>
            </div>
        </div>

        <?php
    }
}

add_filter( 'genesis_post_title_output', 'filter_genesis_post_title_tags', 15 );

function filter_genesis_post_title_tags( $title ) {


    $title = sprintf( '<h2>%s</h2><h1 class="entry-title">%s</h1>', get_field('hero_text') ,apply_filters( 'genesis_post_title_text', get_the_title() ) );

    return $title;

}

genesis();