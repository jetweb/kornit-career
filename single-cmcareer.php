<?php


add_filter( 'body_class','add_career_body_class' );

function add_career_body_class( $classes ) {
    global $post;

    $term = get_the_terms($post, 'department');

    $classes[] = $term[0]->slug;
    return $classes;

}

genesis();
