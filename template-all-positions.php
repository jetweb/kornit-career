<?php

/* Template Name: All Positions */


function do_all_positions(){

    $options = array(
        'post_type' => 'cmcareer',
        'post_status' => 'publish',
        'numberposts' => -1,
        //'exclude' => array(get_field('general_position_id', 'options')->ID)
    );
    $departments_array =  array(
        'taxonomy' => 'department',
        'hide_empty' => true,
        'meta_query' => array(
            array(
                'key'       => 'active',
                'value'     => 1,
                'compare'   => '='
            )
        )
    ) ;

    $departments = get_terms( $departments_array );

    foreach ($departments as $dep) {
        $depArray[] = $dep->slug;
    }


    $locations_array = array(
        'taxonomy' => 'location',
        'hide_empty' => true,
    ) ;

    $options['tax_query'] = array(
        array(
            'taxonomy' => 'department',
            'field' => 'slug',
            'terms' =>  $depArray
        ),
    );


    if (isset($_GET['location']) && $_GET['location'] != "0") {
        $location = get_term($_GET['location']);
        $options['tax_query'] = array(
            array(
                'taxonomy' => 'location',
                'field' => 'slug',
                'terms' => $location->slug,
            ),
        );
        $locations_array['search'] = $location->name;
    }
    if (isset($_GET['svalue']) && $_GET['svalue'] != "") {
        $options['s'] = $_GET['svalue'];
    }

    $careers = get_posts($options);
    $locations = get_terms( $locations_array );
    include (get_stylesheet_directory() . '/views/positions.php');

}

add_action('wp_enqueue_scripts', 'enqueue_all_positions_scripts');

function enqueue_all_positions_scripts(){
    wp_enqueue_script('isotope', get_stylesheet_directory_uri() . '/assets/vendor/isotope.min.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_script('font-awesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/js/all.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_style( 'positions', get_stylesheet_directory_uri() . '/assets/css/department-colors.css' );

}

add_action('genesis_entry_content','do_all_positions');


add_action('genesis_before_entry', 'do_page_hero', 10);

function do_page_hero () {
    global $post;
    $fields = get_fields($post);

    ?>

    <div class="hero" style="background-image: url(<?php echo $fields['header_image']['url'] ?>)">
        <div class="wrap">
            <div class="content">
                <div class="secondary-title"><?php echo $fields['title_1'] ?></div>
                <div class="title"><?php echo $fields['title_2'] ?></div>
            </div>
        </div>
    </div>

    <?php

}


genesis();
