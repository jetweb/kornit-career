<?php

remove_action('genesis_loop','ea_archive_loop');

function do_department_positions(){

    $department = get_query_var('department');

    $options = array(
        'post_type' => 'cmcareer',
        'post_status' => 'publish',
        'numberposts' => -1,
        //'exclude' => array(get_field('general_position_id', 'options')->ID)
    );


    $locations_array = array(
        'taxonomy' => 'location',
        'hide_empty' => true,
    ) ;

    $options['tax_query'] = array(
        array(
            'taxonomy' => 'department',
            'field' => 'slug',
            'terms' =>  $department
        ),
    );


    $careers = get_posts($options);
    $locations = get_terms( $locations_array );


    include (get_stylesheet_directory() . '/views/positions.php');

}

add_action('wp_enqueue_scripts', 'enqueue_all_positions_scripts');

function enqueue_all_positions_scripts(){
    wp_enqueue_script('isotope', get_stylesheet_directory_uri() . '/assets/vendor/isotope.min.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_script('font-awesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/js/all.js', array(), CHILD_THEME_VERSION, false);
    wp_enqueue_style( 'positions', get_stylesheet_directory_uri() . '/assets/css/department-colors.css' );

}

add_action('genesis_footer','do_department_positions', 4);

add_action('genesis_before_entry', 'do_page_hero', 10);

function do_page_hero () {
    global $post;
    $fields = get_fields($post);
    print_r($fields);

    ?>

    <div class="hero" style="background-image: url(<?php echo $fields['header_image']['url'] ?>)">
        <div class="wrap">
            <div class="content">
                <div class="secondary-title"><?php echo $fields['title_1'] ?></div>
                <div class="title"><?php echo $fields['title_2'] ?></div>
            </div>
        </div>
    </div>

    <?php

}





genesis();