jQuery(function($){
	// Mobile Menu
	$('.menu-toggle').click(function(){
		$('.search-toggle, .header-search').removeClass('active');
		$('.menu-toggle, .nav-menu').toggleClass('active');
	});
	$('.menu-item-has-children > .submenu-expand').click(function(e){
		$(this).toggleClass('expanded');
		e.preventDefault();
	});

	// Search toggle
	$('.search-toggle').click(function(){
		$('.menu-toggle, .nav-menu').removeClass('active');
		$('.search-toggle, .header-search').toggleClass('active');
		$('.site-header .search-field').focus();
	});

});


// @codekit-prepend "smoothscroll.js"

jQuery(function ($) {

	updateCount();
	// Mobile menu
	$('.mobile-menu-toggle').click(function () {
		$('body').toggleClass('mobile-menu-expanded');
	});
	$('.submenu-expand').click(function () {
		$(this).parent().toggleClass('submenu-active');
	});

	if ($(window).width() <= 768) {
		jQuery(".nav-menu .menu-item:last-of-type").detach().appendTo('.site-header');
	}
	if ($(window).width() <= 1024) {
		$('.perks-repeater').slick({
			autoplay: true,
			dots: true,
			arrows: false,
			autoplaySpeed: 7000,
		});
	}
	$(".how-we-hire-repeater-mobile .circle-holder").on("click", function () {
		$(this).find(".whitebox-curve").slideToggle();
		$(this).find(".outer-text").slideToggle();
		$(this).toggleClass("slideToggled");
	});
	$(window).on("scroll", function () {
		if ($(window).scrollTop() > 100) {
			//$(".site-header").addClass("scrolled");
		} else {
			//remove the background property so it comes transparent again (defined in your css)
			//$(".site-header").removeClass("scrolled");
		}
	});


	// $("#autocomplete").easyAutocomplete({
	// 	url: globalvars.url,
	// 	ajaxSettings: {
	// 		dataType: "json",
	// 		method: "POST",
	// 		data: {
	// 			action: "retrieve_search_data",
	// 		}
	// 	},
	// 	getValue: "text",
	// 	template: {
	// 		type: "custom",
	// 		method: function (value, item) {
	// 			var location = "";
	// 			var elevel = "";
	// 			var etype = "";
	// 			if(item.location && item.location.name){
	// 				location = '<span class="position-location">' + item.location.name + '</span>';
	// 			}
	// 			if(item.elevel && item.elevel.name){
	// 				elevel = '<span class="position-level">' + item.elevel.name + '</span>';
	// 			}
	// 			if(item.etype && item.etype.name){
	// 				etype = '<span class="position-type">' + item.etype.name + '</span>';
	// 			}
	// 			return '<a href="' + item.websitelink + '"><span class="position-name">' + item.text + '</span>'+location+elevel+etype+'</a>';
	// 		}
	// 	},
	// 	list: {
	// 		onChooseEvent: function () {
	// 			window.location.href = $(".easy-autocomplete li.selected a").attr("href");
	// 		}
	// 	},
	// 	preparePostData: function (data) {
	// 		data.searchVal = $("#autocomplete").val();
	// 		data.location = $('.section-hero .cs-placeholder').text();
	// 		return data;
	// 	},
	// }).on('keypress', function(event){
	// 	var keycode = (event.keyCode ? event.keyCode : event.which);
	// 	if(keycode == '13'){
	// 		var val = $('.cs-selected').data('value');
	// 		if (typeof val == 'undefined') {
	// 			val = 0;
	// 		}
	// 		location.href = "/positions/?location=" + val + "&svalue=" + $('#autocomplete').val();
	// 	}
	// });



	// $('.vision-photos').slick({
	// 	autoplay: true,
	// 	dots: true,
	// 	arrows: false,
	// 	autoplaySpeed: 2000,
	// });
	//
	// $('.gauges.mbl').slick({
	// 	autoplay: true,
	// 	dots: true,
	// 	arrows: false,
	// 	autoplaySpeed: 2000,
	// });
	//
	// var $testSlick = $('.test').slick({
	// 	swipe: false,
	// 	touchMove: false,
	// 	draggable: false,
	// 	dots: true,
	// 	fade: true,
	// 	arrows: false,
	// 	cssEase: 'cubic-bezier(0,1.17,.31,.63)',
	// 	infinite: false,
	// 	speed: 100
	// });

	// $('.test').on('beforeChange', function (slick, currentSlide, nextSlide) {
	// 	$('.slick-slide').addClass('fastExit');
	// });
	// $('.test').on('afterChange', function (slick, currentSlide, nextSlide) {
	// 	$('.slick-slide').removeClass('fastExit');
	// });
	// $('.test .slick-dots li button').on('click', function (e) {
	// 	e.stopPropagation(); // use this
	// });
	// var totalPoints = 0;
	// $('.answer-button').on('click', function (e) {
	// 	e.preventDefault();
	// 	var thisStep = $(this).closest('.step').attr('data-slick-index');
	// 	totalPoints += parseInt($(this).attr('data-answer-loc'));
	// 	$('.test').slick('slickGoTo', parseInt(thisStep)+1, false);
	// });
	//
	//
	//
	// var slidesToNews = 3;
	// if ($(window).width() <= 1360 && $(window).width() > 1024) {
	// 	slidesToNews = 2;
	// }
	// else if ($(window).width() <= 1024) {
	// 	slidesToNews = 1;
	// }
	// $('.news-repeater').slick({
	// 	centerMode: true,
	// 	centerPadding: '10px',
	// 	slidesToScroll: 1,
	// 	slidesToShow: slidesToNews,
	// 	dots: true
	// });
	// var maxHeight = 0;
	// $('.news-article').each(function(i, obj) {
	// 	if($(this).height() > maxHeight){
	// 		maxHeight = $(this).height();
	// 	}
	// });
	// $('.news-article').height(maxHeight);
	//
	// $('.blue-button').on('touchstart', function(){
	// 	$(this).closest('.flip-container').toggleClass('hover');
	// });

//    var maxHeight = 0;
//    $('.page-template-template-about .flip-container .front').each(function(){
//        $(this).css('position', 'relative');
//        if(maxHeight < $(this).outerHeight()){
//            console.log($(this).outerHeight());
//            console.log($(this).innerHeight());
//            console.log($(this).height());
//            maxHeight = $(this).outerHeight();
//        }
//        $(this).css('position', 'absolute');
//    });
//
//    $('.page-template-template-about .flip-container .back').each(function(){
//        if(maxHeight < $(this).outerHeight()){
//            maxHeight = $(this).outerHeight();
//        }
//    });
//    $('.flip-container').css('height', maxHeight);
//    $('.front').css('height', maxHeight);
//    $('.back').css('height', maxHeight);

// filter functions



	$('.testimonials-actual').slick({
		autoplay: true,
		dots: true,
		arrows: false,
		autoPlaySpeed: 6000
	});
	if ($('.home').length > 0) {
		var $careersIsotope = $('.home .careers-container').isotope({
			itemSelector: '.element-item',
			layoutMode: 'vertical',
			filter: isotopeFilter,
			resizeContainer: true
		});
	}

	var isotopeFilter = '*';
	if ($('.careers-actual-wrapper').attr('data-initial') != "") {
		isotopeFilter = '.' + $('.careers-actual-wrapper').attr('data-initial');
	}

// init Isotope
	if ($('.page-template-template-positions').length > 0) {
		var $careersIsotope = $('.page-template-template-positions .careers-container').isotope({
			itemSelector: '.element-item',
			layoutMode: 'fitRows',
			filter: isotopeFilter,
			resizeContainer: true,
			getSortData: {
				department: '[data-department]',
				location: '[data-location]',
			},
			sortBy: ['department', 'location']
		});
	}

	if($careersIsotope){
		$careersIsotope.on('arrangeComplete', function (event, filteredItems) {


			if (filteredItems.length === 0) {
				console.log('No Results');


				if ($(".no-results").length == 0) {
					$('.no-results').remove();
					$('.careers-container').append('<div id="no-results-wrapper"><div class="no-results">No results</div><div class="trigger-reset-filters"><a href="#" class="trigger-reset-filters-actual">Reset Filters</a></div></div>');
					$('.trigger-reset-filters-actual').on('click', function(e){
						e.preventDefault();
						$('.careers-controller-container > a:first-of-type').each(function() { $(this).trigger('click')});
						$('.careers-controller-container').each(function() { $(this).val('*').trigger('change')});
						$('#no-results-wrapper').remove();
					});
				}

			}

		});
	}

	$('.sort-by-radio').change(function () {
		if ($(this).attr('data-type') === "department") {
			$careersIsotope.isotope({sortBy: ['department', 'location']});
		} else {
			$careersIsotope.isotope({sortBy: ['location', 'department']});
		}
	});


// bind filter button click
	$('.careers-controller').on('click', function (e) {
		e.preventDefault();
		$('#no-results-wrapper').remove();
		/*
        $('.no-results').remove();
		$('.trigger-reset-filters').remove();
		*/

		var $this = $(this);
		console.log($(this).closest('.careers-controller-container'));
		$(this).closest('.careers-controller-container').find('.active').removeClass('active');
		$this.addClass('active');
		var filters = '';
		$('.careers-controller.active').each(function () {
			if ($(this).attr('data-filter') != '*') {
				filters = filters + $(this).attr('data-filter');
			}
		});
		if (filters.length == 0) {
			filters = '*';


		}

		console.log(filters);
		$careersIsotope.isotope({filter: filters});
		updateCount();

	});

	$('.careers-controller-container').change(function () {
		var filters = '';
		$('#no-results-wrapper').remove();
		/*
        $('.no-results').remove();
		 $('.trigger-reset-filters').remove();
		 */

		$('.careers-controller-container').each(function () {
			filters = filters + $(this).val();
		});
		console.log(filters);
		$careersIsotope.isotope({filter: filters});
		updateCount();
	});

	$('.explore-button').on('click', function (e) {
		e.preventDefault();
		/*location.href="/positions/?location="+$('.cs-placeholder').text()+"&svalue="+$('#autocomplete').val();*/
		var val = $('.cs-selected').data('value');
		if (typeof val == 'undefined') {
			val = 0;
		}
		location.href = "/positions/?location=" + val + "&svalue=" + $('#autocomplete').val();
	});

//    $("#autocomplete").keydown(function (e) {
//        if (($(".easy-autocomplete li.selected").length == 0) && (e.keyCode == 13))
//        {
//            var val = $('.cs-selected').data('value');
//            if (typeof val == 'undefined') {
//                val = 0;
//            }
//
//            location.href = "/positions/?location=" + val + "&svalue=" + $('#autocomplete').val();
//        }
//    });

	$(".info-holder").each(function () {
		$(this).html($(this).html().replace(/Must/g, "<span class='must'>MUST</span>"));
	});



	var alreadyDone = false;
	if($('.home').length > 0){
		var waypoint = new Waypoint({
			element: document.getElementById('section-gauges'),
			handler: function (direction) {
				if (!alreadyDone) {
					$('.gauge-loader').css('transition', 'initial');
					$('.gauge-loader').css('transform', 'rotate(46deg)');
					setTimeout(moveToEnd, 100);
					alreadyDone = true;
				}
			},
			offset: '50%'
		});
	}

	function moveToEnd() {
		$('.gauge').each(function (i, obj) {
			var $this = $(this);
			var speed = $this.attr('data-speed');
			$this.find('.gauge-loader').css('transition', 'all ' + speed + 'ms');
			$this.find('.gauge-loader').css('transform', 'rotate(215deg)');
		});
	}

});


(function () {
	[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
		new SelectFx(el);
	});
})();











var width = jQuery(window).width();
var height = width / 1.778;

//var videoSection = jQuery('.video_header');
//videoSection.height(jQuery( window ).height());
//videoSection.width('100%');

var windowWidth = window.innerWidth;
// Load the IFrame Player API code asynchronously.
console.log("windowsWidth: " + windowWidth);
if(windowWidth >= 1024){
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/player_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;
var players = [];
var videos;
var tempPlayer;

function onYouTubePlayerAPIReady() {
	$ = jQuery;
	/* main video*/
	player = new YT.Player('ytplayer', {
		height: '100%',
		width: '100%',
		wmode: 'transparent',
		//playerVars: {'autoplay': 1, loop: 1, 'controls': 0, 'rel': 0, 'showinfo': 0, 'autohide': 1, 'wmode': 'transparent'/* , 'listType':'playlist', 'list': listid*/},
		playerVars: {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3},
		videoId: '' + jQuery("#ytplayer").attr("data-vid"),
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange

		}
		//jQuery("#ytplayer").attr("data-vid")
	});

}
var isFirst = true;

function onPlayerReady(event) {

	player.mute();
	player.playVideo();

	jQuery('.video-header .video-play').on('click', function () {
		playVideo();
	});
	jQuery('.video-header .video-pause').on('click', function () {
		pauseVideo();
	});

	jQuery('.show-cursor').on('click', function () {
		var isPlaying = jQuery('.video-play').hasClass('d-none');
		var isMuted = player.isMuted();
		if (isMuted) { // first time user pressed play button -> play video from 0 + unmute
			playVideo();
		} else if (isPlaying && !isMuted) {
			pauseVideo();
		} else {
			playVideo();
		}

	});
}

function playVideo() {
	if (isFirst) {
		isFirst = false;
		player.unMute();
		player.seekTo(0);
	}
	jQuery('.video-header .video-container').removeClass('filter-brightness');
	jQuery('.video-header .details').addClass('d-none');

	showHideElement('.video-pause');
	showHideElement('.video-play', false);
	player.playVideo();
}

function pauseVideo() {
	jQuery('.video-header .video-container').addClass('filter-brightness');
	jQuery('.video-header .details').removeClass('d-none');
	showHideElement('.video-pause', false);
	showHideElement('.video-play');
	player.pauseVideo();
}

function onPlayerStateChange(event) {
	if(event.data === 0) {
		player.seekTo(0);
	}
}

updateCount = function () {
	var locationSlug = jQuery('.positions-container.locations .careers-controller.active').data('filter');
	var departmentSlug = jQuery('.careers-controller-container.departments .careers-controller.active').data('filter');
	let selector_loc = '';
	let selector_dep = '';
	if(locationSlug == '*'){
		selector_loc = '';
	}else{
		selector_loc = locationSlug;
	}
	if(departmentSlug == '*'){
		selector_dep = '';
	}else{
		selector_dep = departmentSlug;
	}
	let selector = selector_loc + selector_dep;
	let departmentSelector = '';
	let locationSelector = '';
	if ((locationSlug == '*') && (departmentSlug == '*') ){
		departmentSelector = '';
		locationSelector = '';
	} else if((locationSlug == '*')){
		departmentSelector = '';
		locationSelector = selector_dep;
	} else if((departmentSlug == '*')){
		departmentSelector = selector_loc;
		locationSelector = '';
	} else {
		departmentSelector = selector;
		locationSelector = selector;
	}
	jQuery('.departments .careers-controller ').each(function (i, obj) {
		var termslug = $(this).data('termslug');
		if ('undefined' != typeof (termslug)) {
			l = $('.careers-container .element-item.' + termslug +departmentSelector).length;
			$(this).find('span.count').text(l);
			if (0 === l){
				$(this).find('span.count').parent().addClass('empty');
			} else {
				$(this).find('span.count').parent().removeClass('empty');
			}
		}
	});
	jQuery('.locations .careers-controller ').each(function (i, obj) {
		var termslug = $(this).data('termslug');
		if ('undefined' != typeof (termslug)) {
			l = $('.careers-container .element-item.' + termslug +locationSelector).length;
			$(this).find('span.count').text(l);
			if (0 === l){
				$(this).find('span.count').parent().addClass('empty');
			} else {
				$(this).find('span.count').parent().removeClass('empty');
			}
		}
	});
};

