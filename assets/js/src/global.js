jQuery(function ($) {

   // updateCount();
    var $grid = $('.careers-container').isotope({
        itemSelector: '.position',
        layoutMode: 'fitRows'
    });


    var iso = $grid.data('isotope');
    var $filterCount = $('.filter-count');


// bind filter button click
    $('.positions-controller').on( 'click', '.button', function() {
        var filterValue = $( this ).attr('data-filter');
        $grid.isotope({ filter: filterValue });
        //updateCount();
    });

    function updateFilterCount() {
        $filterCount.text( '(' + iso.filteredItems.length + ')' );
    }

    updateFilterCount();

// change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });



});


updateCount = function () {
    var locationSlug = 'locations'; // jQuery('.positions-container.locations .careers-controller.active').data('filter');   //TODO populate dynamically
    var departmentSlug = 'departmnets' // jQuery('.careers-controller-container.departments .careers-controller.active').data('filter'); //TODO populate dynamically
    let selector_loc = '';
    let selector_dep = '';
    if(locationSlug == '*'){
        selector_loc = '';
    }else{
        selector_loc = locationSlug;
    }
    if(departmentSlug == '*'){
        selector_dep = '';
    }else{
        selector_dep = departmentSlug;
    }
    let selector = selector_loc + selector_dep;
    let departmentSelector = '';
    let locationSelector = '';
    if ((locationSlug == '*') && (departmentSlug == '*') ){
        departmentSelector = '';
        locationSelector = '';
    } else if((locationSlug == '*')){
        departmentSelector = '';
        locationSelector = selector_dep;
    } else if((departmentSlug == '*')){
        departmentSelector = selector_loc;
        locationSelector = '';
    } else {
        departmentSelector = selector;
        locationSelector = selector;
    }
    jQuery('.departments.positions-controller').each(function (i, obj) {
        var termslug = jQuery(this).data('termslug');
        if ('undefined' != typeof (termslug)) {
            l = jQuery('.careers-container .element-item.' + termslug +departmentSelector).length;
            jQuery(this).find('span.count').text(l);
            if (0 === l){
                jQuery(this).find('span.count').parent().addClass('empty');
            } else {
                jQuery(this).find('span.count').parent().removeClass('empty');
            }
        }
    });
    jQuery('.locations .positions-controller ').each(function (i, obj) {
        var termslug = $(this).data('termslug');
        if ('undefined' != typeof (termslug)) {
            l = $('.careers-container .element-item.' + termslug +locationSelector).length;
            $(this).find('span.count').text(l);
            if (0 === l){
                $(this).find('span.count').parent().addClass('empty');
            } else {
                $(this).find('span.count').parent().removeClass('empty');
            }
        }
    });
};