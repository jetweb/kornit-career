<?php
/**
 * Functions
 *
 * @package      KornitCareer
 * @author       Roy Hoshen
 **/
define( 'CHILD_THEME_VERSION', '0.0.1' );

/**
 * Set up the content width value based on the theme's design.
 *
 */
if (!isset($content_width))
    $content_width = 768;

/**
 * Global enqueues
 *
 * @since  1.0.0
 * @global array $wp_styles
 */
function ea_global_enqueues()
{

    // javascript
    if (!ea_is_amp()) {
        //wp_enqueue_script( 'ea-global', get_stylesheet_directory_uri() . '/assets/js/global-min.js', array( 'jquery' ), filemtime( get_stylesheet_directory() . '/assets/js/global-min.js' ), true );
        wp_enqueue_script('ea-global', get_stylesheet_directory_uri() . '/assets/js/src/global.js', array('jquery'), CHILD_THEME_VERSION, true);

        // Move jQuery to footer
        if (!is_admin()) {
            wp_deregister_script('jquery');
            wp_register_script('jquery', includes_url('/js/jquery/jquery.js'), false, NULL, true);
            wp_enqueue_script('jquery');
        }

    }

    // css
    wp_dequeue_style('child-theme');
    wp_register_style('ea-fonts', ea_theme_fonts_url());
    wp_register_style('ea-critical', get_stylesheet_directory_uri() . '/assets/css/critical.css', array(), filemtime(get_stylesheet_directory() . '/assets/css/critical.css'));
    wp_register_style('ea-style', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), filemtime(get_stylesheet_directory() . '/assets/css/main.css'));

    if ($using_critical_css = true) {
        wp_enqueue_style('ea-critical');
        wp_dequeue_style('wp-block-library');
        add_action('wp_footer', 'ea_enqueue_noncritical_css', 1);
    } else {
        ea_enqueue_noncritical_css();
    }

}

add_action('wp_enqueue_scripts', 'ea_global_enqueues');

/**
 * Enqueue Non-Critical CSS
 *
 */
function ea_enqueue_noncritical_css()
{
    wp_enqueue_style('wp-block-library');
    wp_enqueue_style('ea-critical');
    wp_enqueue_style('ea-style');
    wp_enqueue_style('ea-fonts');
}

/**
 * Gutenberg scripts and styles
 *
 */
function ea_gutenberg_scripts()
{
    wp_enqueue_style('ea-fonts', ea_theme_fonts_url());
    wp_enqueue_script('ea-editor', get_stylesheet_directory_uri() . '/assets/js/editor.js', array('wp-blocks', 'wp-dom'), filemtime(get_stylesheet_directory() . '/assets/js/editor.js'), true);
}

add_action('enqueue_block_editor_assets', 'ea_gutenberg_scripts');

/**
 * Theme Fonts URL
 *
 */
function ea_theme_fonts_url()
{

    return false;
}

/**
 * Theme setup.
 *
 * Attach all of the site-wide functions to the correct hooks and filters. All
 * the functions themselves are defined below this setup function.
 *
 * @since 1.0.0
 */
function ea_child_theme_setup()
{
	

    // General cleanup
    include_once(get_stylesheet_directory() . '/inc/wordpress-cleanup.php');
    include_once(get_stylesheet_directory() . '/inc/genesis-changes.php');

    // Theme
    include_once(get_stylesheet_directory() . '/inc/markup.php');
    include_once(get_stylesheet_directory() . '/inc/helper-functions.php');
    include_once(get_stylesheet_directory() . '/inc/layouts.php');
    include_once(get_stylesheet_directory() . '/inc/navigation.php');
    include_once(get_stylesheet_directory() . '/inc/loop.php');
    include_once(get_stylesheet_directory() . '/inc/author-box.php');
    include_once(get_stylesheet_directory() . '/inc/template-tags.php');
    include_once(get_stylesheet_directory() . '/inc/site-footer.php');

    include_once(get_stylesheet_directory() . '/inc/department-colors.php');

    include_once(get_stylesheet_directory() . '/lib/cpt-testimonial.php');

    // Editor
    include_once(get_stylesheet_directory() . '/inc/disable-editor.php');
    include_once(get_stylesheet_directory() . '/inc/tinymce.php');

    // Functionality
    include_once(get_stylesheet_directory() . '/inc/login-logo.php');
    //include_once(get_stylesheet_directory() . '/inc/block-area.php');
    include_once(get_stylesheet_directory() . '/inc/social-links.php');

    // Plugin Support
    include_once(get_stylesheet_directory() . '/inc/acf.php');
    include_once(get_stylesheet_directory() . '/inc/amp.php');
    include_once(get_stylesheet_directory() . '/inc/shared-counts.php');
    include_once(get_stylesheet_directory() . '/inc/wpforms.php');
	
        include_once( get_stylesheet_directory() . '/inc/updater.php' );

    // Editor Styles
    add_theme_support('editor-styles');
    add_editor_style('assets/css/editor-style.css');

    // Image Sizes
    // add_image_size( 'ea_featured', 400, 100, true );

    // Gutenberg

    // -- Responsive embeds
    add_theme_support('responsive-embeds');

    // -- Wide Images
    add_theme_support('align-wide');

    // -- Disable custom font sizes
    add_theme_support('disable-custom-font-sizes');

    // -- Editor Font Styles
    add_theme_support('editor-font-sizes', array(
        array(
            'name' => __('Small', 'ea_genesis_child'),
            'shortName' => __('S', 'ea_genesis_child'),
            'size' => 14,
            'slug' => 'small'
        ),
        array(
            'name' => __('Normal', 'ea_genesis_child'),
            'shortName' => __('M', 'ea_genesis_child'),
            'size' => 20,
            'slug' => 'normal'
        ),
        array(
            'name' => __('Large', 'ea_genesis_child'),
            'shortName' => __('L', 'ea_genesis_child'),
            'size' => 24,
            'slug' => 'large'
        ),
    ));

    // -- Disable Custom Colors
    add_theme_support('disable-custom-colors');

    // -- Editor Color Palette
    add_theme_support('editor-color-palette', array(
        array(
            'name' => __('Blue', 'ea_genesis_child'),
            'slug' => 'blue',
            'color' => '#05306F',
        ),
        array(
            'name' => __('Grey', 'ea_genesis_child'),
            'slug' => 'grey',
            'color' => '#FAFAFA',
        ),
    ));

}

add_action('genesis_setup', 'ea_child_theme_setup', 15);

/**
 * Change the comment area text
 *
 * @param array $args
 * @return array
 * @since  1.0.0
 */
function ea_comment_text($args)
{
    $args['title_reply'] = __('Leave A Reply', 'ea_genesis_child');
    $args['label_submit'] = __('Post Comment', 'ea_genesis_child');
    $args['comment_notes_before'] = '';
    $args['comment_notes_after'] = '';
    return $args;
}

add_filter('comment_form_defaults', 'ea_comment_text');


/**
 * Template Hierarchy
 *
 */
function ea_template_hierarchy($template)
{
    if (is_home())
        $template = get_query_template('archive');
    return $template;
}

add_filter('template_include', 'ea_template_hierarchy');


add_action("edited_department", 'update_departments_colors_css', 10, 0);

add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');


function update_department_page_relation($value, $post_id, $field)
{
    update_field('r_page', $post_id, $value);
}

//add_filter('acf/update_value/name=r_page', 'update_department_page_relation', 10, 3);
add_filter('acf/update_value/name=r_department', 'update_department_page_relation', 10, 3);


// Removes from admin menu
add_action('admin_menu', 'my_remove_admin_menus');
function my_remove_admin_menus()
{
    remove_menu_page('edit-comments.php');
}

// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support()
{
    remove_post_type_support('post', 'comments');
    remove_post_type_support('page', 'comments');
}

// Removes from admin bar
function korc_admin_bar_render()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}

add_action('wp_before_admin_bar_render', 'korc_admin_bar_render');


function my_login_logo_one() {
    ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri() . '/assets/images/logo.jpg' ?>);
            padding-bottom: 30px;
            margin-bottom: 10px;
            width: 100%;
            background-size: cover;
        }
    </style>
    <?php
}


add_action('login_enqueue_scripts', 'my_login_logo_one');