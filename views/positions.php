<?php


?>

    <div class="positions-conatiner">
        <div class="wrap">
            <div class="controller-container">
                <div class="categories">
                    <?php
                    if (!is_tax('department')){
                        echo '<h4>Departments</h4>';

                        if (!wp_is_mobile()) {
                            echo '<div class="positions-controller departments">';
                            echo '<a href="#" class="button" data-filter="*">All (' . count($careers) . ')</a>';

                            foreach ($departments as $department) {
                                echo '<a href="#" class="button" data-filter=".' . $department->slug . '">' . $department->name . ' <span class="filter-count"></span></a>';
                            }
                            echo '</div>';
                        } else {
                            echo '<select class="positions-controller departments">';
                            echo '<option class="positions-controller" value="*">All (' . count($careers) . ')</option>';
                            foreach ($departments as $department) {

                                echo '<option class="careers-controller" value=".' . $department->slug . '">' . $department->name . ' (' . $department->count . ')</option>';
                            }
                            echo '</select>';
                        }
                        echo '</div>';
                    }
                    ?>
                    <h4>Locations</h4>
                    <?php
                    if (!wp_is_mobile()) {
                        echo '<div class="positions-controller locations">';
                        echo '<a href="#" class="button" data-filter="*">All (' . count($careers) . ')</a>';

                        foreach ($locations as $location) {
                            //$fields = get_field('comeet_country', $location);
                            echo '<a href="#" class="button" data-filter=".' . $location->slug . '">' . $location->name . ' (<span class="count"></span>)</a>';
                        }
                        echo '</div>';
                    } else {
                        echo '<select class="positions-controller locations">';
                        echo '<option class="" value="*">All (' . count($careers) . ')</option>';
                        foreach ($locations as $location) {

                            echo '<option class="" value=".' . $location->slug . '">' . $location->name . ' (' . $location->count . ')</option>';
                        }
                        echo '</select>';
                    }
                    echo '</div>';

                    ?>
                    <div class="careers-container">
                        <?php
                        if ($careers):
                            foreach ($careers as $career) {
                                $clocation = wp_get_post_terms($career->ID, 'location')[0];
                                $cdepartment = wp_get_post_terms($career->ID, 'department')[0];
                                $cetype = wp_get_post_terms($career->ID, 'employment_type')[0];
                                $celevel = wp_get_post_terms($career->ID, 'experience_level')[0];
                                ?>
                                <a href="<?php echo get_permalink($career->ID); ?>"
                                   class="position <?php echo $clocation->slug; ?> <?php echo $cdepartment->slug; ?> <?php echo $cetype->slug; ?> <?php echo $celevel->slug; ?>"
                                   data-department="<?php echo $cdepartment->slug; ?>"
                                   data-location="<?php echo $clocation->slug; ?>">
                                    <div class="info">
                                        <div class="department"><?php echo $cdepartment->name; ?></div>
                                       <div class="title"><?php echo $career->post_title; ?></div>
                                       <div class="additional"><?php echo $cetype->name; ?> , <?php echo $celevel->name; ?></div>

                                    <div class="location"><?php echo $clocation->name; ?><i class="fas fa-map-marker-alt"></i></div>
                                    </div>
                                </a>
                                <?php
                            }
                        else:
                            echo '<span class="careers-no-results">No results</span>';
                        endif;
                        ?>
                    </div>
                </div>
            </div>
    </div>

<?php


