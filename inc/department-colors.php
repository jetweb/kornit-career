<?php


function update_departments_colors_css()
{
    $terms = get_terms('department');
    ob_start();

    foreach ($terms as $term) {
        $color = get_field('colors', $term);

        if ($color != "") {

            ?>

            a.position.<?php echo $term->slug ?>:hover {
                border: 2px solid <?php echo $color['accent']; ?>;
                box-shadow: 0 0 0 0 <?php echo $color['accent']; ?>;
            }

            a.position.<?php echo $term->slug ?> .info .department {
                color: <?php echo $color['accent']; ?>;
            }


            <?php
        }
    }


    $css = ob_get_clean();
    file_put_contents(get_stylesheet_directory() . '/assets/css/department-colors.css', $css);
}



