<?php

/*
 * 
 *  Jet_Update File for plugins:
 * Usage:
 *  1. Include this file.
 *  2. new Jet_Update(); 
 *  3. When upload the plugin to update.jetweb.co.il the slug name must be the name of the plugin file.
 */


if (!class_exists('Jet_Update_Theme')):

    class Jet_Update_Theme {

        private $api_url, $theme_data, $theme_version, $theme_base;

        function __construct($api_url = 'http://updates.jetweb.co.il') {
            if (function_exists('wp_get_theme')) {
                $this->theme_data = wp_get_theme(get_option('stylesheet'));
                $this->theme_version = $this->theme_data->Version;
                $this->theme_base = strtolower(get_option('stylesheet'));
            } else {
                return;
            }
            $this->api_url = $api_url;

            add_filter('pre_set_site_transient_update_themes', array($this, 'check_for_update'));
            add_filter('themes_api', array($this, 'my_theme_api_call'), 10, 3);
        }

        public function check_for_update($checked_data) {
            global $wp_version;

            $request = array(
                'slug' => $this->theme_base,
                'version' => $this->theme_version
            );

            $send_for_check = array(
                'body' => array(
                    'action' => 'theme_update',
                    'request' => serialize($request),
                    'api-key' => md5(get_bloginfo('url'))
                ),
                'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
            );

            $raw_response = wp_remote_post($this->api_url, $send_for_check);
            $response = '';


            if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)) {
                $response = unserialize($raw_response['body']);
            }

            if (!empty($response)) {
                $checked_data->response[$this->theme_base] = $response;
            }

            return $checked_data;
        }

        public function my_theme_api_call($def, $action, $args) {
            if ($args->slug != $this->theme_base) {
                return false;
            }

            $args->version = $this->theme_version;
            $request_string = prepare_request($action, $args);
            $request = wp_remote_post($api_url, $request_string);

            if (is_wp_error($request)) {
                $res = new WP_Error('themes_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
            } else {
                $res = unserialize($request['body']);

                if ($res === false) {
                    $res = new WP_Error('themes_api_failed', __('An unknown error occurred'), $request['body']);
                }
            }

            return $res;
        }

    }
  new Jet_Update_Theme();
 
endif;